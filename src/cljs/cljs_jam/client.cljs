(ns cljs-jam.client
  (:require [cljs-jam.gfx :as gfx]
            [cljs-jam.sprite :as sprite]
            [cljs-jam.events :as events]
            [cljs-jam.font :as font]
            [cljs-jam.sound :as sound]
            [cljs.core.async :refer [put! chan <! >! alts! timeout close!]]
            [cljs-jam.utils :refer [log]]
            [dommy.core :as dommy :refer-macros [sel1]]
            [clojure.browser.repl :as repl]
            )
  (:require-macros [cljs.core.async.macros :refer [go alt!]]))


(def world (gfx/init :background 0x202040
                     :expand true
                     :engine :canvas
                     :canvas (sel1 :#mycanvas)))

(def stage (:world world))
(def ui (:ui world))

(def open-sans (font/make-tiled-font "Open Sans" 400 15))

(defn load [s urls & {:keys [fade-in fade-out]
                      :or {fade-in 0.5 fade-out 0.5}
                      :as options}]
  (let [c (chan)
        b (gfx/add-prog-bar s options)]
    (go
      ;; fade in
      (<! (gfx/fadein b :duration fade-in))

      ;; show load progress
      (log "load" (str options))
      (<! (apply (partial gfx/load-urls urls b) options))

      ;; load is done. return message
      (>! c true)

      ;; delay a tiny bit
      (<! (timeout 300))

      ;; fadeout
      (<! (gfx/fadeout b :duration fade-out))

      ;; remove progress bar sprite
      (.removeChild s b)

      (close! c))
    c))

(def stars (atom #{}))
(def rocks (atom #{}))
(def players (atom nil))

(def score (atom 0))

(def explosion-frames (for [a [0 64 128 192] b [0 64 128 192]] [b a 64 64]))

(def explosion-frame-textures (for [[x y w h] explosion-frames]
                      (->
                       (js/PIXI.Texture.fromImage "img/explosion.png")
                       (gfx/sub-texture [x y] [w h])
                       )))

(defn set-frame! [sp frame]
  (.setTexture sp (nth explosion-frame-textures frame))
)

;; setup just a frame renderer loop
(events/clear-frame-chans!)
(let [c (events/new-frame-chan)]
  (go
    (while true
      (<! c)
      (.render (:renderer world) (:stage world)))))

(defn star-spawner "spawn stars"
  [num delay]
  (go
    (let [blip (<! (sound/load-sound "sfx/blip.ogg"))]
      (loop [i num]
        (when (not (nil? @players))
          (let [type (if (< (rand) 0.2)
                       :rock
                       :star)
                storage (case type
                          :rock rocks
                          :star stars)
                sprite (gfx/make-sprite
                        (case type
                          :rock (js/PIXI.Texture.fromImage "img/rock-medium.png")
                          :star (js/PIXI.Texture.fromImage "img/star.png"))
                      :x (- (rand 2000) 1000)
                      :y (- (rand 2000) 1000))
                r1 (+ 0.5 (* 0.5 (rand)))]

            (doto sprite
              (sprite/set-scale! r1)
              (sprite/set-rotation! (* 10 (rand))))

            ;; add to stored stars and make it appear
            (swap! storage conj sprite)

            (.addChild stage sprite)
            (sound/play-sound blip 0.06)

            (<! (timeout delay))
            (when (pos? i)
              (recur (dec i)))))))))


;; barrage of explosions
(defn barrage [num delay]
  (go
    (let [boom-sfx (<! (sound/load-sound "sfx/explosion.ogg"))
          explosion (fn [x y]
                      (let [ex (gfx/make-sprite (nth explosion-frame-textures 0) :x x :y y)]
                        (.addChild stage ex)
                        (sound/play-sound boom-sfx 0.2)
                        (go (loop [f 0]
                              (set-frame! ex f)
                              (<! (timeout 60))
                              (when (< f 15) ;; 16 frames
                                (recur (inc f))))

                            (.removeChild stage ex))))

          fireworks (fn [num delay]
                      (go
                        (loop [left (dec num)]
                          (explosion (- (rand 800) 400) (- (rand 500) 250))
                          (<! (timeout delay))
                          (when (pos? left)
                            (recur (dec left))))))]

      (fireworks num delay))))


;; typewriter
(defn typewriter
  "control: the control channel. send it :messages
  sayings: a list of saying strings
  font: a tiled font structure"
  [sayings font]
  (let [control (chan)]
    (go
      (let [sfx (<! (sound/load-sound "sfx/keypress.ogg"))
            typed-text (font/font-make-batch font " ")]
        (.addChild ui (:sprite typed-text))

         ;; pin to lower section
              (go
                (let [c (events/new-resize-chan)]
                  (while true
                      (let [w (.-innerWidth js/window)
                            h (.-innerHeight js/window)
                            hw (/ w 2)
                            hh (/ h 2)]
                        (sprite/set-pos! (:sprite typed-text) 0 (* hh (/ 2 3)))
                        (<! c)))))

        (loop []
          (alt!
            ;; control message handler
            control
            ([message]
             (if (= message :quit)
               ;quit
               (.removeChild ui (:sprite typed-text))

               ;any other message is ignored
               (recur)))

            ;; text typing routine
            (events/next-frame)
            (do
              (let [saying (rand-nth sayings) ]
                (loop [text saying pos [
                                        (- (/ (font/font-text-width font saying) 2))
                                        0]]
                  (<! (timeout 60))
                  (when (not (empty? text))

                    ;; key sound effect
                    (when (not= (first text) " ")
                      (sound/play-sound sfx 0.1))

                    (recur (rest text)
                           (font/batch-add typed-text pos (str (first text)))))))
              (<! (timeout 3000))
              ;; remove characters

              (.removeChildren (:sprite typed-text) 0 100)
              (<! (timeout 10000))
              (recur))))))

    control))


(if true
  (go
    (let [
          dummy (font/make-tiled-font "Open Sans" 400 15)
          ;fmetrics (<! (font/load-font-js-metrics "400 Open Sans" 24))
          loader (load ui [
                           "http://fonts.gstatic.com/s/opensans/v10/cJZKeOuBrn4kERxqtaUH3VtXRa8TVwTICgirnJhmVJw.woff2" "http://fonts.gstatic.com/s/opensans/v10/cJZKeOuBrn4kERxqtaUH3T8E0i7KZn-EPnyo3HZu7kw.woff"
                           "img/explosion.png"
                           "sfx/explosion.ogg"
                           "sfx/keypress.ogg"
                           "sfx/blip.ogg"
                           "sfx/pickup.ogg"
                           "img/star.png"
                           "img/rock-medium.png"
                           "http://www.goodboydigital.com/pixijs/bunnymark/bunny.png"
                       ]
                :full-colour 0x306020
                :highlight 0x80ff80
                :lowlight 0x103010
                :empty-colour 0x000000
                :debug-delay 0.1
                :fade-in 0.2
                :fade-out 2)]

      ;; wait for bar to finish
      (<! loader)

      (go (let [
                                        ;sound (multi-sound "sfx/keypress" 5 50)



                  _ (<! (events/next-frame))
                  _ (<! (events/next-frame))
                  _ (<! (events/next-frame))
                  _ (<! (timeout 1000))
                font (font/make-tiled-font "Open Sans" 400 20)
                  ]

              (let [st (font/font-make-batch font "Take it to the Fuckin Limit!")
                    scr (font/font-make-batch font (str "Score: " @score))]
                ;; (set! (.-position.x (:sprite score)) 300)

                (log "USING" (str scr) ui (:sprite scr))
                (.addChild ui (:sprite scr))

                (add-watch score :add
                           (fn [key ref old new]
                             (do
                               (.removeChildren (:sprite scr) 0 100)
                               (font/batch-add scr [0 0] (str "Score: " new)))
                             ))

                ;; anchor score to top left
                (go
                  (let [c (events/new-resize-chan)]
                    (while true
                      (let [w (.-innerWidth js/window)
                            h (.-innerHeight js/window)
                            hw (/ w 2)
                            hh (/ h 2)]
                        (sprite/set-pos! (:sprite scr) (- 5 hw) (- 5 hh))
                        (<! c)))))

                (<! (timeout 1000))

                (barrage 13 200)

                ;; start the spawner for 500 stars, 1.25 sec delay
                (star-spawner 500 1250))))


      ;; player appears
      (let [player (gfx/make-sprite (js/PIXI.Texture.fromImage "http://www.goodboydigital.com/pixijs/bunnymark/bunny.png")
                                    )
            font (font/make-tiled-font "Open Sans" 400 20)
            pickup (<! (sound/load-sound "sfx/pickup.ogg"))
            boom-sfx (<! (sound/load-sound "sfx/explosion.ogg"))
            sayings [
                               "That's one small fart for bunny, one giant poop for bunnykind."
                               "Take it to the fucking limit!"
                               "This is the most amazing Experimental Typewriter"
                               "One must be careful of colliding with Uranus"
                               "I love the smell of the vacuum of space"
                               "What else is there to say ?"
                               "A cheese burger with fries"
                               "If an electron deorbits and no one is there to see it, does it emit a photon?"
                               "I miss the earth so much, I miss my wife."
                               "I have enough oxygen for 5 hours."
                               "Do dee doo"
                               "...in space... no one can hear you fart!"
                               "going boldly where no bunny has gone before"
                               "I'm sorry, Dave. I'm afraid I can't do that."
                               "The Universe can wait"
                               "It's full of stars"
                               "I'll be in my ready-room."
                               "Are you the keymaster?"
                               "It had been a wonderful evening and what I needed now, to give it a perfect ending, was a little of the Ludwig Van."
                               "Help me, Obi-wan Kenobi. You're my only hope."
                               "E.T. phone home…"
                               "I am your father"
                               "I want to learn the ways of the force and become a Jedi, like my father."
                               ]
            writer (typewriter sayings font)
]
        (.addChild stage player)
        (swap! players (fn [_] player))

        ;; wait for fadeout and remove of loading dialog
        (<! loader)

        ;; spaceship bunny
        (loop [fnum 0 theta Math/PI [x y] [0 0] [vx vy] [0 0]]
          (doto player
            (sprite/set-rotation! (+ theta Math/PI))
            (sprite/set-pos! x y))

          ;; move camera based on last value of camera and player position
          (let [
                px (.-pivot.x stage)
                py (.-pivot.y stage)
                ]
            (sprite/set-pivot!
             stage
             (+ px (* 0.03 (- x px)))
             (+ py (* 0.03 (- y py)))))

          ;; tick
          (<! (events/next-frame))

          ;; collision detection
          (let [current-stars @stars]
            (doseq [star current-stars]
              (when (sprite/overlap? star player)
                ;; sprite pop and sound
                (swap! stars disj star)

                (sound/play-sound pickup 0.4)

                ;; popup score from picking up star
                (go
                  (<! (events/next-frame))

                  (let [points (* 10 (int (* 50 (.-scale.x star))))
                        _ (log star)
                        x-original (sprite/get-x star)
                        y-original (sprite/get-y star)
                        score-text (font/font-make-batch font (str points)
                                                   :x 0
                                                   :y 0
                                                   )
                        score-sprite (:sprite score-text)]

                    (doto score-sprite
                      (sprite/set-pos! x-original y-original)
                      (sprite/set-pivot! 25 15))

                    (.addChild stage score-sprite)

                    ;; add to score
                    (swap! score + points)

                    (loop [i 20]
                      (set! (.-position.y score-sprite) (+ y-original (- i 20)))
                      (<! (events/next-frame))
                      (if (pos? i)
                        (recur (dec i))

                        (loop [i 20]
                          (doto score-sprite
                            ;; make fainter
                            (sprite/set-alpha! (/ i 20))

                            ;; and bigger
                            (sprite/set-scale! (+ 1 (* 0.1 (- 20 i)))))

                          (<! (events/next-frame))
                          (if (pos? i)
                            (recur (dec i))

                            (.removeChild stage score-sprite)))))
                    )

                  ;; remove
                  )
                (.removeChild stage star)
                )

              ))


          ;; if up is pressed, emit a smoke sprite
          (when (and
                                        ; every 3
                                        ;(= 0 (mod fnum 3))

                                        ; 50%
                 (< (rand) 0.5)

                 (events/is-pressed? :up))

            (go
              (let [smoke
                    (gfx/make-sprite
                     (js/PIXI.Texture.fromImage
                      (nth ["img/smoke-0.png"
                            "img/smoke-1.png"
                            "img/smoke-2.png"
                            "img/smoke-3.png"] (rand-int 4)))
                     :x (+ x (* 20 (Math/sin theta)))
                     :y (+ y (* -20 (Math/cos theta))) y)]

                (doto smoke
                  (sprite/set-scale! 0.1)
                  (sprite/set-rotation! (* 100 (rand))))
                (.addChildAt stage smoke 0)

                (loop [i 500]
                  (let [fu (- 1 (/ i 500))
                        fd (/ i 500)]
                                        ;(log "->" i fu fd)
                    (doto smoke
                      (sprite/set-scale! (+ 0.01 (* fu 0.5)))
                      (sprite/set-alpha! (* fd fd fd fd fd))
                      )
                    )
                  (<! (events/next-frame))
                  (when (pos? i)
                    (recur (dec i))
                    ))
                (.removeChild stage smoke)
                )))

          ;; exit
          (let [dead? (let [current-rocks @rocks]
                        (loop [remaining current-rocks]
                          (if (not (empty? remaining))
                            (if (sprite/overlap? (first remaining) player)
                              ;; a collision was found
                              true

                              ;; keep searching
                              (recur (rest remaining))
                              )

                            ;; not found
                            false)))]

            (if (or
                 (events/is-pressed? :q)
                 dead?)
              ;; exit recur block to continue with go thread
              true

              (recur
               ;; frame number
               (inc fnum)

               ;; angle
               (+ theta
                  (if (events/is-pressed? :left)
                    -0.04
                    (if (events/is-pressed? :right)
                      0.04
                      0)))

               ;; new position
               [(+ x vx)
                (+ y vy)]

               ;; new velocity
               [(* 0.99
                   (+ vx (if (events/is-pressed? :up)
                           (* -0.2 (Math/sin theta))
                           0)))
                (* 0.99
                   (+ vy (if (events/is-pressed? :up)
                           (* 0.2 (Math/cos theta))
                           0)
                      0.01))]))))

        ;; player has rage-quit
        (go
          ;; stop typewriter
          ;;(go (>! writer :quit))           ;; >! blocks while typeriter is in frame loop
          ;; or
          (put! writer :quit)


          ;; explode bunny
          (sound/play-sound boom-sfx)
          (loop [i 0]
            (set-frame! player i)
            (<! (timeout 60))
            (when (< i 15)
              (recur (inc i))))


          ;; wait 1 sec
          (<! (timeout 1000))

          ;; remove player
          (.removeChild stage player)

          ;; player is niled?
          (swap! players (fn [] nil))

          ;; wait 4 seconds
          (<! (timeout 4000))

          ;; game over
          (let [game-over (:sprite (font/font-make-batch font "GAME OVER" :x 0 :y 0))]
            (doto game-over
              (sprite/set-pivot! 50 10))
            (.addChild ui game-over))
          )

        ;; slowly center camera on explosion position
        (loop [x (sprite/get-x player)
               y (sprite/get-y player)]
          (<! (events/next-frame))
          (let [

                px (.-pivot.x stage)
                py (.-pivot.y stage)
                ]
            (sprite/set-pivot!
             stage
             (+ px (* 0.015 (- x px)))
             (+ py (* 0.015 (- y py)))))
          (recur x (+ y 0.25))
          )))))
