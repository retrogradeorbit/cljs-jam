(defproject cljs-jam "0.1.0-SNAPSHOT"
  :description "FIXME: write this!"
  :url "http://example.com/FIXME"
  :dependencies [[org.clojure/clojure "1.6.0"]
                 [org.clojure/clojurescript "0.0-2511"]
                 [ring "1.3.0"]
                 [org.clojure/core.async "0.1.338.0-5c5012-alpha"]
                 [prismatic/dommy "1.0.0"]
                 [garden "1.1.7"]
                 [hiccup "1.0.5"]
                 [com.cemerick/clojurescript.test "0.3.1"]
                 [com.cemerick/piggieback "0.1.3"]
                 [cider/cider-nrepl "0.9.0-SNAPSHOT"]
]
  :plugins [[lein-cljsbuild "1.0.3"]
            [lein-ring "0.8.10"]
            [com.cemerick/clojurescript.test "0.3.1"]]
  :hooks [leiningen.cljsbuild]
  :source-paths ["src/clj"]
  :cljsbuild
  {
   :builds
   {
    :dev {
          :cache-analysis true
          :source-paths ["src/cljs"]
          :incremental true
          :compiler {
                     :output-to "resources/public/js/game.js"
                     :output-dir "resources/public/js/"
                     :source-map "resources/public/js/game.js.map"
                     :optimizations :simple ;:whitespace ;:simple
                     :pretty-print true
                     :externs ["src/js/pixi/extern.js"
                               "src/js/font.js/extern.js"]
                     }}
    }}
  :main cljs-jam.server
  :ring {:handler cljs-jam.server/app}
  :aliases {"html" ["run" "-m" "cljs-jam.server/-write-files"]}
  :repl-options {:nrepl-middleware [cemerick.piggieback/wrap-cljs-repl]}
)
